export class ConditionVM {

    public ID : number;

    public machineVelocity : number;

    public windVelocity : number;

    public windDirection : number;

    constructor(conditionObject: Partial<ConditionVM>) {
        Object.assign(this, conditionObject);
    }
    
}