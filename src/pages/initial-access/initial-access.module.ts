import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InitialAccessPage } from './initial-access';

@NgModule({
  declarations: [
    InitialAccessPage,
  ],
  imports: [
    IonicPageModule.forChild(InitialAccessPage),
  ],
})
export class InitialAccessPageModule {}
