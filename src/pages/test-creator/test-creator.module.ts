import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestCreatorPage } from './test-creator';

@NgModule({
  declarations: [
    TestCreatorPage,
  ],
  imports: [
    IonicPageModule.forChild(TestCreatorPage),
  ],
})
export class TestCreatorPageModule {}
