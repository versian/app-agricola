import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '../../services/user-service/user-service';
import { RequestReturn } from '../../viewmodels/RequestReturn';
import { UserVM } from '../../viewmodels/UserVM';
import { AppState } from '../../app/app.state';
import { MessageType } from '../../enums/MessageType';
import { SHA256 } from '../../util/SHA256';


@Injectable()
export class UserDataservice {

  constructor(private userService: UserService, private appState: AppState) {
  }


  public async isFirstAccessDone(): Promise<RequestReturn<boolean>> {

    let result: RequestReturn<boolean>;

    return this.userService.getAll()
      .then(users => {

        let firstAccessDone: boolean = false;

        users.forEach((user, index, array) => {
          firstAccessDone = (user.lastLogon == null) ? false : true;
        });

        result.message = firstAccessDone ? "Primeiro acesso já foi feito." : "Primeiro acesso ainda não foi feito.";
        result.messageTitle = "Primeiro Acesso";
        result.messageType = firstAccessDone ? MessageType.SUCCESS : MessageType.ERROR;
        result.returnObject = firstAccessDone

        return result;

      })
      .catch(error => {
        console.log(error);

        result.message = "Ocorreu um erro ao buscar os dados. Detalhes do erro: " + error;
        result.messageTitle = "Erro";
        result.messageType = MessageType.ERROR;
        result.returnObject = false;

        return result;
      });

  }

  public async getUserData(userID: number): Promise<RequestReturn<UserVM>> {

    let result: RequestReturn<UserVM>;

    return this.userService.getByID(userID)
      .then(user => {
        result.message = "Dados do usuário retornados com sucesso!";
        result.messageTitle = "Usuário";
        result.messageType = MessageType.SUCCESS;
        result.returnObject = user;

        return result;
      })
      .catch(error => {
        console.log(error);

        result.message = "Ocorreu um erro ao buscar os dados do usuário. Detalhes do erro: " + error;
        result.messageTitle = "Erro";
        result.messageType = MessageType.ERROR;
        result.returnObject = error;

        return result;
      });
  }

  public async getLoggedUser(): Promise<RequestReturn<UserVM>> {

    let result: RequestReturn<UserVM>;

    return this.userService.getAll()
      .then(users => {

        let userVM: UserVM;

        users.forEach((user, index, array) => {
          if (user.loggedHash != null)
            userVM = user;
        });

        result.message = userVM == null ? "Não há nenhum usuário logado no sistema." : "Há um usuário logado no sistema.";
        result.messageTitle = "Usuário Logado";
        result.messageType = MessageType.SUCCESS;
        result.returnObject = userVM;

        return result;

      })
      .catch(error => {
        console.log(error);

        result.message = "Ocorreu um erro ao buscar os dados. Detalhes do erro: " + error;
        result.messageTitle = "Erro";
        result.messageType = MessageType.ERROR;
        result.returnObject = error;

        return result;
      });
  }

  public async setUserData(user: UserVM): Promise<RequestReturn<boolean>> {

    let result: RequestReturn<boolean>;

    return this.userService.update(user)
      .then(insertReturn => {

        result.message = insertReturn ? "Informações atualizadas com sucesso!" : "Ocorreu um erro ao atualizar os dados.";
        result.messageTitle = insertReturn ? "Salvar Informações" : "Erro";
        result.messageType = insertReturn ? MessageType.SUCCESS : MessageType.ERROR;
        result.returnObject = insertReturn;

        return result;
      })
      .catch(error => {
        console.log(error);

        result.message = "Ocorreu um erro ao atualizar as informações.";
        result.messageTitle = "Erro";
        result.messageType = MessageType.ERROR;
        result.returnObject = error;

        return result;
      });
  }

  public async loginUser(username: string, pwd: string): Promise<RequestReturn<boolean>> {

    let requestReturn: RequestReturn<boolean>;

    return this.userService.getAll()
      .then(users => {

        let userVM: UserVM;
        let time = Date.now();
        let validUser: boolean = false;

        users.forEach((user, index, array) => {
          if (user.username === username)
            userVM = user;
        });

        let secretHash = SHA256(userVM.username + userVM.passwordHash + time);

        if (secretHash === SHA256(username + SHA256(pwd) + time))
          validUser = true;

        if (validUser) {
          userVM.loggedHash = secretHash;

          this.appState.userVM = userVM;
          this.appState.userVM.lastLogon = new Date(time);
          this.appState.loggedUserHashToken = secretHash;

          return this.userService.update(userVM)
            .then(result => {

              requestReturn.message = result ? "Login feito com sucesso!" : "Ocorreu um erro ao realizar o login.";
              requestReturn.messageTitle = "Login";
              requestReturn.messageType = result ? MessageType.SUCCESS : MessageType.ERROR;
              requestReturn.returnObject = result;

              return requestReturn;

            })
            .catch(error => {
              requestReturn.message = "Ocorreu um erro ao realizar o login.";
              requestReturn.messageTitle = "Login";
              requestReturn.messageType = MessageType.ERROR;
              requestReturn.returnObject = false;

              return requestReturn;
            });
        } else {
          requestReturn.message = "Usuário ou senha inválidas.";
          requestReturn.messageTitle = "Login";
          requestReturn.messageType = MessageType.ERROR;
          requestReturn.returnObject = false;

          return requestReturn;
        }

      })
      .catch(error => {
        console.log(error);

        requestReturn.message = "Ocorreu um erro ao buscar os dados. Detalhes do erro: " + error;
        requestReturn.messageTitle = "Erro";
        requestReturn.messageType = MessageType.ERROR;
        requestReturn.returnObject = error;

        return requestReturn;
      });
  }

  public async logoffUser(): Promise<RequestReturn<boolean>> {

    let requestReturn: RequestReturn<boolean>;

    this.appState.userVM.loggedHash = null;
    this.appState.loggedUserHashToken = null;

    return this.userService.update(this.appState.userVM)
    .then(result => {

      requestReturn.message = result ? "Logoff feito com sucesso!" : "Ocorreu um erro ao realizar o logoff.";
      requestReturn.messageTitle = result ? "Logoff" : "Erro";
      requestReturn.messageType = result ? MessageType.SUCCESS : MessageType.ERROR;
      requestReturn.returnObject = result;

      return requestReturn;

    })
    .catch(error => {

      requestReturn.message = "Ocorreu um erro ao realizar o logoff.";
      requestReturn.messageTitle = "Erro";
      requestReturn.messageType = MessageType.ERROR;
      requestReturn.returnObject = false;

      return requestReturn;

    });
  }

  public async registerUser(user: UserVM): Promise<RequestReturn<boolean>> {

    let requestReturn: RequestReturn<boolean>;

    return this.userService.insert(user)
      .then(result => {

        requestReturn.message = result ? "Usuário cadastrado com sucesso!" : "Ocorreu um erro ao realizar o cadastro.";
        requestReturn.messageTitle = result ? "Cadastro" : "Erro";
        requestReturn.messageType = result ? MessageType.SUCCESS : MessageType.ERROR;
        requestReturn.returnObject = result;

        return requestReturn;

      })
      .catch(error => {

        console.log(error);

        requestReturn.message = "Ocorreu um erro ao realizar o cadastro.";
        requestReturn.messageTitle = "Erro";
        requestReturn.messageType = MessageType.ERROR;
        requestReturn.returnObject = error;

        return requestReturn;

      });
  }

  public recoverPassword(email: string): RequestReturn<boolean> {
    return null;
  }

}