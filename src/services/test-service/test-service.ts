import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatabaseService } from '../database-service/database-service';
import { TestVM } from '../../viewmodels/TestVM';
import { RegulationVM } from '../../viewmodels/RegulationVM';
import { ConditionVM } from '../../viewmodels/ConditionVM';
import { ProductVM } from '../../viewmodels/ProductVM';
import { UserVM } from '../../viewmodels/UserVM';


@Injectable()
export class TestService {

  constructor(private databaseService: DatabaseService) {
  }


  public getAll(): Promise<TestVM[]> {
    return this.databaseService.getDB()
      .then(db => {

        return db.executeSql(`SELECT t.*, c.*, r.*, u.* FROM Test as t
                              INNER JOIN Condition as c on c.ID == t.conditionID
                              INNER JOIN Regulation as r on r.ID == t.regulationID
                              INNER JOIN Product as p on p.ID == t.productID
                              INNER JOIN User as u on u.ID == t.userID`, {})
          .then(data => {

            return data.rows.items.map(function (test) {
              db.executeSql("SELECT * FROM Condition WHERE id = ?", [test.conditionID]).then(conditionReturn => {
                db.executeSql("SELECT * FROM Regulation WHERE id = ?", [test.regulationID]).then(regulationReturn => {
                  db.executeSql("SELECT * FROM Product WHERE id = ?", [test.productID]).then(productReturn => {
                    db.executeSql("SELECT * FROM User WHERE id = ?", [test.userID]).then(userReturn => {

                      let condition = conditionReturn.rows.items(0);
                      let regulation = regulationReturn.rows.items(0);
                      let product = productReturn.rows.items(0);
                      let user = userReturn.rows.items(0);

                      let conditionVM = new ConditionVM({ 
                          ID: condition.id, 
                          machineVelocity: condition.machineVelocity, 
                          windVelocity: condition.windVelocity, 
                          windDirection: condition.windDirection 
                        });

                      let regulationVM = new RegulationVM({ 
                        ID: regulation.id, 
                        palletePosition: regulation.palletePosition, 
                        deflectorPosition: regulation.deflectorPosition, 
                        plateRPM: regulation.plateRPM, 
                        plateDiameter: regulation.plateDiameter 
                      });

                      let productVM = new ProductVM({ 
                        ID: test.id, 
                        comercialName: test.comercialName, 
                        manufacturer: test.manufacturer, 
                        granulometry: test.granulometry 
                      });

                      let userVM = new UserVM({ 
                        ID: user.id, 
                        username: user.username, 
                        passwordHash: user.passwordHash, 
                        name: user.name, 
                        birthday: user.birthday, 
                        RG: user.RG, 
                        CPF: user.CPF, 
                        lastLogon: user.lastLogon 
                      });

                      return new TestVM({ 
                        ID: test.ID, 
                        tax: test.tax, 
                        doses: test.doses, 
                        qtdMachines: test.qtdMachines, 
                        collectorWidth: test.collectorWidth, 
                        collectorLength: test.collectorLength, 
                        repetitionsNumber: test.repetitionsNumber,
                        regulation: regulationVM,
                        condition: conditionVM,
                        product: productVM,
                        user: userVM 
                      });
                    })
                  })
                })
              })
            })
          });
      });
  }

  public getByID(id: number): Promise<TestVM> {
    return this.databaseService.getDB()
      .then(db => {

        return db.executeSql("SELECT * FROM Test WHERE id = ? LIMIT 1", [id]).then(testReturn => {

            return testReturn.rows.items.map(function (test) {
              db.executeSql("SELECT * FROM Condition WHERE id = ?", [test.conditionID]).then(conditionReturn => {
                db.executeSql("SELECT * FROM Regulation WHERE id = ?", [test.regulationID]).then(regulationReturn => {
                  db.executeSql("SELECT * FROM Product WHERE id = ?", [test.productID]).then(productReturn => {
                    db.executeSql("SELECT * FROM User WHERE id = ?", [test.userID]).then(userReturn => {

                      let condition = conditionReturn.rows.items(0);
                      let regulation = regulationReturn.rows.items(0);
                      let product = productReturn.rows.items(0);
                      let user = userReturn.rows.items(0);

                      let conditionVM = new ConditionVM({ 
                          ID: condition.id, 
                          machineVelocity: condition.machineVelocity, 
                          windVelocity: condition.windVelocity, 
                          windDirection: condition.windDirection 
                        });

                      let regulationVM = new RegulationVM({ 
                        ID: regulation.id, 
                        palletePosition: regulation.palletePosition, 
                        deflectorPosition: regulation.deflectorPosition, 
                        plateRPM: regulation.plateRPM, 
                        plateDiameter: regulation.plateDiameter 
                      });

                      let productVM = new ProductVM({ 
                        ID: test.id, 
                        comercialName: test.comercialName, 
                        manufacturer: test.manufacturer, 
                        granulometry: test.granulometry 
                      });

                      let userVM = new UserVM({ 
                        ID: user.id, 
                        username: user.username, 
                        passwordHash: user.passwordHash, 
                        name: user.name, 
                        birthday: user.birthday, 
                        RG: user.RG, 
                        CPF: user.CPF, 
                        lastLogon: user.lastLogon 
                      });

                      return new TestVM({ 
                        ID: test.ID, 
                        tax: test.tax, 
                        doses: test.doses, 
                        qtdMachines: test.qtdMachines, 
                        collectorWidth: test.collectorWidth, 
                        collectorLength: test.collectorLength, 
                        repetitionsNumber: test.repetitionsNumber,
                        regulation: regulationVM,
                        condition: conditionVM,
                        product: productVM,
                        user: userVM 
                      });
                      
                    }).catch(error => { return null; });
                  }).catch(error => { return null; });
                }).catch(error => { return null; });
              }).catch(error => { return null; });
            }).catch(error => { return null; });
          }).catch(error => { return null; });
      }).catch(error => { return null; });
  }

  public insert(test: TestVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(async db => {
        let sql = [];
        let conditionID: number;
        let productID: number;
        let regulationID: number;

        if (test.condition.ID == null) {
          conditionID = await db.executeSql("SELECT MAX(ID) + 1 FROM Condition", {}).then(result => { return result.rows.items(0) });
          sql.push(["INSERT INTO Condition VALUES(?, ?, ?, ?)", [conditionID, test.condition.machineVelocity, test.condition.windVelocity, test.condition.windDirection]]);
        } else {
          conditionID = test.condition.ID;
        }

        if (test.product.ID == null) {
          productID = await db.executeSql("SELECT MAX(ID) + 1 FROM Product", {}).then(result => { return result.rows.items(0) });
          sql.push(["INSERT INTO Product VALUES(?, ?, ?, ?)", [productID, test.product.comercialName, test.product.manufacturer, test.product.granulometry]]);
        } else {
          productID = test.product.ID;
        }

        if (test.regulation.ID == null) {
          regulationID = await db.executeSql("SELECT MAX(ID) + 1 FROM Regulation", {}).then(result => { return result.rows.items(0) });
          sql.push(["INSERT INTO Regulation VALUES(?, ?, ?, ?, ?)", [regulationID, test.regulation.palletePosition, test.regulation.deflectorPosition, test.regulation.plateDiameter, test.regulation.plateRPM]]);
        } else {
          regulationID = test.regulation.ID;
        }

        sql.push(["INSERT INTO Test VALUES((SELECT MAX(ID) + 1 FROM Test), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [test.tax, test.doses, test.qtdMachines, test.collectorWidth, test.collectorLength, test.repetitionsNumber, regulationID, conditionID, productID, test.user.ID]]);

        return db.sqlBatch(sql).then(result => { return true; }).catch(error => { return false; });

      })
      .catch(error => { return false; });
  }

  public update(test: TestVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {
        let sql = [
          ['UPDATE Condition SET machineVelocity = ?,  windVelocity = ?, windDirection = ? WHERE id = ?', [test.condition.machineVelocity, test.condition.windVelocity, test.condition.windDirection, test.condition.ID]],
          ['UPDATE Regulation SET palletePosition = ?, deflectorPosition = ?, plateRPM = ?, plateDiameter = ? WHERE id = ?', [test.regulation.palletePosition, test.regulation.deflectorPosition, test.regulation.plateRPM, test.regulation.plateDiameter, test.regulation.ID]],
          ['UPDATE Product SET comercialName = ?, manufacturer = ?, granulometry = ? WHERE id = ?', [test.product.comercialName, test.product.manufacturer, test.product.granulometry, test.product.ID]],
          ['UPDATE Test SET tax = ?, doses = ?, qtdMachines = ?, collectorWidth = ?, collectorLength = ?, repetitionsNumber = ? WHERE id = ?', [test.tax, test.doses, test.qtdMachines, test.collectorWidth, test.collectorLength, test.repetitionsNumber, test.ID]]
        ];

        return db.sqlBatch(sql).then(result => { return true; }).catch(error => { return false; });
      })
      .catch(error => { return false; });
  }

  public remove(test: TestVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(async db => {
        let sql = [
          ['DELETE Test WHERE id = ?', [test.ID]]
        ];

        return db.sqlBatch(sql).then(result => { return true; }).catch(error => { return false; });
      })
      .catch(error => { return false; });
  }

}
