import { Injectable } from '@angular/core';
import { DatabaseService } from '../database-service/database-service';
import { UserVM } from '../../viewmodels/UserVM';
import { SQLiteObject } from '@ionic-native/sqlite';
import { LicenseVM } from '../../viewmodels/LicenseVM';


@Injectable()
export class LicenseService {

  constructor(private databaseService: DatabaseService) {
  }


  public getLicense(): Promise<LicenseVM> {
    return this.databaseService.getDB()
      .then(db => {

        return db.executeSql('SELECT * FROM License LIMIT 1', {})
          .then(data => {

            let license = data.rows.items(0);
            return new LicenseVM({ 
              farmName: license.farmName, 
              CNPJ: license.CNPJ, 
              ownerEmail: license.ownerEmail, 
              ownerName: license.ownerName, 
              ownerCPF: license.ownerCPF,
              licenseDate: license.licenseDate, 
              licenseHash: license.licenseHash,
              licenseCode: license.licenseCode, 
              licenseValidate: license.licenseValidate 
            });
          
          }).catch(() => { return null; });
      }).catch(() => { return null});
  }

  public insert(license: LicenseVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {

        var sql = 'INSERT INTO License VALUES(?, ?, ?, ?, ?, ?, ?, ?)';
        var data = [
          license.farmName,
          license.CNPJ,
          license.ownerName,
          license.ownerEmail,
          license.licenseCode,
          license.licenseDate,
          license.licenseHash,
          license.licenseValidate
        ];

        return db.executeSql(sql, data)
          .then(() => { return true; })
          .catch(error => { return false; });

      })
      .catch(error => { return false; });
  }

  public update(license: LicenseVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {

        var sql = 'UPDATE License SET licenseCode = ?, licenseDate = ?, licenseHash = ?, licenseValidate = ? WHERE farmName = ? AND CNPJ = ?';
        var data = [
          license.licenseCode,
          license.licenseDate,
          license.licenseHash,
          license.licenseValidate,
          license.farmName,
          license.CNPJ
        ];

        return db.executeSql(sql, data)
          .then(() => { return true; })
          .catch(error => { return false; });

      })
      .catch(error => { return false; });
  }

  public remove(license: LicenseVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {

        var sql = 'DELETE FROM License WHERE farmName = ? AND CNPJ = ?';
        var data = [license.farmName, license.CNPJ];

        return db.executeSql(sql, data)
          .then(() => { return true; })
          .catch(error => { return false; });

      })
      .catch(error => { return false; });
  }

}
