import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LicenseValidationPage } from './license-validation';

import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    LicenseValidationPage,
  ],
  imports: [
    IonicPageModule.forChild(LicenseValidationPage),
    BrMaskerModule
  ],
})
export class LicenseValidationPageModule {}
