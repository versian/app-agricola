import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { LicenseService } from '../../services/license-service/license-service';
import { RequestReturn } from '../../viewmodels/RequestReturn';
import { LicenseVM } from '../../viewmodels/LicenseVM';
import { AppState } from '../../app/app.state';
import { Constants } from '../../enums/Constants';


@Injectable()
export class LicenseDataservice {

  constructor(private licenseService: LicenseService, private appState: AppState, private http: Http) {
  }


  public isSystemLicensed(): RequestReturn<boolean> {
    let license;

    this.licenseService.getLicense().then(result => {
      license = result;

      if(license != null) {
        
      }
    })

    return null;
  }

  public licenseSystem(license: LicenseVM): RequestReturn<boolean> {
    
    this.http.post(Constants.API_BASE_ADDRESS + "endereco", license).toPromise()
    .then(result => {

    })
    .catch(error => {

    });
    
    return null;
  }

}