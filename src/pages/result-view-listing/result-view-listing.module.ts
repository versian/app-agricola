import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultViewListingPage } from './result-view-listing';

@NgModule({
  declarations: [
    ResultViewListingPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultViewListingPage),
  ],
})
export class ResultViewListingPageModule {}
