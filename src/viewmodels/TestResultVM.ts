import { TestVM } from "./TestVM";
import { TestResult } from "./TestResult";

export class TestResultVM {

    public TestSource: TestVM;
    
    public TestResult : TestResult[];
}