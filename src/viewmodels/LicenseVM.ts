export class LicenseVM {
    
    public farmName: string;

    public CNPJ: string;

    public ownerEmail: string;

    public ownerName: string;

    public ownerCPF: string;

    public licenseDate: Date;

    public licenseHash: string;

    public licenseCode: string;

    public licenseValidate: string;

    constructor(licenseObject: Partial<LicenseVM>) {
        Object.assign(this, licenseObject);
    }
}