export class EmailVM {

    public from: string;

    public to: string;
    
    public subject: string;

    public body: string;

    constructor(emailObject: Partial<EmailVM>) {
        Object.assign(this, emailObject);
    }

}