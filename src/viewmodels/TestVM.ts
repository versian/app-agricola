import { Tax } from "../enums/Tax";
import { RegulationVM } from "./RegulationVM";
import { ConditionVM } from "./ConditionVM";
import { ProductVM } from "./ProductVM";
import { UserVM } from "./UserVM";

export class TestVM {
    
    public ID: number;

    public tax: Tax;

    public doses: number;

    public qtdMachines: number;

    public collectorWidth: number;

    public collectorLength: number;

    public repetitionsNumber: number;

    public regulation: RegulationVM;

    public condition: ConditionVM;

    public product: ProductVM;

    public user: UserVM;


    constructor(testObject: Partial<TestVM>) {
        Object.assign(this, testObject);
    }
    
}