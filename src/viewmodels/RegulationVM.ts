export class RegulationVM {
    
    public ID: number;

    public palletePosition: number;

    public deflectorPosition: number;

    public plateRPM: number;

    public plateDiameter: number;


    constructor(regulationObject: Partial<RegulationVM>) {
        Object.assign(this, regulationObject);
    }
    
}