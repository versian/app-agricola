import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestReturn } from '../../viewmodels/RequestReturn';
import { EmailVM } from '../../viewmodels/EmailVM';
import { TestVM } from '../../viewmodels/TestVM';
import { AppState } from '../../app/app.state';


@Injectable()
export class ReportsDataservice {

  constructor(private appState: AppState) {}


  public sendEmail(email: EmailVM, test: TestVM): RequestReturn<boolean> {
    return null;
  }

  public exportTestToPDF(test: TestVM): RequestReturn<boolean> {
    return null;
  }

}