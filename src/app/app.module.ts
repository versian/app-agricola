import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { AppState } from './app.state';
import { DatabaseService } from '../services/database-service/database-service';
import { TestService } from '../services/test-service/test-service';
import { UserService } from '../services/user-service/user-service';
import { LicenseService } from '../services/license-service/license-service';
import { UserDataservice } from '../dataservices/user-dataservice/user-dataservice';
import { LicenseDataservice } from '../dataservices/license-dataservice/license-dataservice';
import { TestDataservice } from '../dataservices/test-dataservice/test-dataservice';
import { ReportsDataservice } from '../dataservices/reports-dataservice/reports-dataservice';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SQLite,
    DatabaseService,
    TestService,
    UserService,
    LicenseService,
    UserDataservice,
    LicenseDataservice,
    TestDataservice,
    ReportsDataservice,
    AppState
  ]
})
export class AppModule {}
