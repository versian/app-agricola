import { Injectable } from '@angular/core';
import { SHA256 } from '../util/SHA256';
import { UserVM } from '../viewmodels/UserVM';

@Injectable()
export class AppState {

  public userVM: UserVM;
  public loggedUserHashToken: string;

  public validateLoggedUser(): boolean {
    if(SHA256(this.userVM.username + this.userVM.passwordHash + this.userVM.lastLogon.getDate()) === this.loggedUserHashToken)
      return true;
    else
      return false;
  }

}
