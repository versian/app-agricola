import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InitialAccessPage } from '../pages/initial-access/initial-access';
import { DatabaseService } from '../services/database-service/database-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'InitialAccessPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, databaseService: DatabaseService) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#FFFFFF');
      databaseService.createTables().then(result => {
        splashScreen.hide();
      });
    });
  }
}

