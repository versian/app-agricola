import { MessageType } from "../enums/MessageType";

export class RequestReturn<T> {
    
    public messageTitle: string;

    public message: string;

    public messageType: MessageType;

    public returnObject: T;

    
    constructor(requestObject: Partial<RequestReturn<T>>) {
        Object.assign(this, requestObject);
    }
    
}