import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LicenseValidationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-license-validation',
  templateUrl: 'license-validation.html',
})
export class LicenseValidationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  license(validation) {
    console.log(validation);
  }

}
