import { Granulometry } from "../enums/Granulometry";

export class ProductVM {
    
    public ID: number;

    public comercialName: string;

    public manufacturer: string;

    public granulometry: Granulometry;

    
    constructor(productObject: Partial<ProductVM>) {
        Object.assign(this, productObject);
    }
}