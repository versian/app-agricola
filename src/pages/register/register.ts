import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';

import { UserDataservice } from '../../dataservices/user-dataservice/user-dataservice';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public statusBar: StatusBar, public userDataService: UserDataservice) {
  }

  register(user) {
    this.userDataService.registerUser(null).then(result => {
      console.log(result);
    })
    .catch(e => {
      console.log(e);
    });
  }

  ionViewWillEnter() {
    this.statusBar.styleLightContent();
    this.statusBar.backgroundColorByHexString('#FF9800');
  }

  ionViewWillLeave() {
    this.statusBar.styleDefault();
    this.statusBar.backgroundColorByHexString('#FFFFFF');
  }

}
