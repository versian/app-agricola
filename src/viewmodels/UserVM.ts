export class UserVM {
    
    public ID: number;

    public username: string;

    public passwordHash: string;

    public name: string;

    public birthday: Date;

    public RG: string;

    public CPF: string;

    public lastLogon: Date;

    public loggedHash: string;


    constructor(userObject: Partial<UserVM>) {
        Object.assign(this, userObject);
    }
    
}