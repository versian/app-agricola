import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';

import { UserDataservice } from '../../dataservices/user-dataservice/user-dataservice';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public userDataService: UserDataservice, public alertCtrl: AlertController) {
    
  }

  login(user) {
    this.userDataService.loginUser(user.user, user.password).then(result => {
      this.navCtrl.setRoot('HomePage');
    }).catch(e => {
      console.log(e);
    });
  }

  recoveryPassword() {
    let alert = this.alertCtrl.create({
      title: 'Recuperar senha',
      inputs: [
        {
          name: 'user',
          type: 'text',
          placeholder: 'Usuário'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Enviar',
          handler: data => {
            if(!data.user){
              let alert = this.alertCtrl.create({
                title: 'Erro',
                message: 'É necessário um usário válido',
                buttons: ['OK']
              });
              alert.present();
            }
            else{
              
            }
          }
        }
      ]
    });
    alert.present();
  }

}
