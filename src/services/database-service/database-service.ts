import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


@Injectable()
export class DatabaseService {

  constructor(private dbConnection: SQLite) {}


  public getDB(): Promise<SQLiteObject> {
    return this.dbConnection.create({
      name: 'agricola.db',
      location: 'default'
    });
  }

  public createTables(): Promise<boolean> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        
        return db.sqlBatch([
          [`CREATE TABLE IF NOT EXISTS License(
            farmName VARCHAR(50), 
            CNPJ CHAR(15), 
            licenseDate DATE, 
            ownerEmail VARCHAR(50), 
            ownerName VARCHAR(50),
            ownerCPF CHAR(15),
            licenseHash CHAR(64),
            licenseCode CHAR(32),
            licenseValidate DATE)`],

          [`CREATE TABLE IF NOT EXISTS User(
            id INT PRIMARY KEY, 
            username VARCHAR(30), 
            passwordHash CHAR(64), 
            name VARCHAR(50), 
            birthday DATE, 
            RG CHAR(9), 
            CPF CHAR(11),
            lastLogon DATE,
            loggedHash CHAR(64))`],
          
          [`CREATE TABLE IF NOT EXISTS Regulation(
            id INT PRIMARY KEY, 
            palletePosition FLOAT, 
            deflectorPosition FLOAT,
            plateRPM INT, 
            plateDiameter FLOAT)`],
          
          [`CREATE TABLE IF NOT EXISTS Condition(
            id INT PRIMARY KEY, 
            machineVelocity FLOAT, 
            windVelocity FLOAT, 
            windDirection FLOAT)`],
          
          [`CREATE TABLE IF NOT EXISTS Product(
            id int PRIMARY KEY, 
            comercialName VARCHAR(30), 
            manufacturer VARCHAR(30),
            granulometry INT)`],
          
          [`CREATE TABLE IF NOT EXISTS Test(
            id int PRIMARY KEY, 
            tax INT, doses INT, 
            qtdMachines INT, 
            collectorWidth INT, 
            collectorLength INT, 
            repetitionsNumber INT, 
            regulationID INT, 
            conditionID INT, 
            productID INT, 
            userID INT, 
            
            FOREIGN KEY(regulationID) REFERENCES Regulation(ID), 
            FOREIGN KEY(conditionID) REFERENCES Condition(ID), 
            FOREIGN KEY(productID) REFERENCES Product(ID), 
            FOREIGN KEY(userID) REFERENCES User(ID))`]
        ])
        .then(() => { return true })
        .catch(() => { return false; });
        
      })
      .catch(e => {
        console.log(e);
        return false;
      })
  }

}
