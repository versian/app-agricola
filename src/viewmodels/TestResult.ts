export class TestResult {

    public collectorNumber : number;

    public cv_right : number;

    public cv_left : number;

    public dataResult : number[];
}