export class DateUtil {

    public toShortDateString(date: Date): string {
        return date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }

    public fromShortDateString(date: string): Date {
        if(date.indexOf('/') > -1) {
            var dateTemp = date.split('/');
            return new Date(dateTemp[2] + '-' + dateTemp[1] + '-' + dateTemp[0] + ' 00:00:00');
        }
        else {
            return new Date(date + ' 00:00:00');
        }
    }

    public toShortTimeString(date: Date): string {
        return ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
    }
    
}