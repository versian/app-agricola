import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TestService } from '../../services/test-service/test-service';
import { RequestReturn } from '../../viewmodels/RequestReturn';
import { TestVM } from '../../viewmodels/TestVM';
import { TestResultVM } from '../../viewmodels/TestResultVM';
import { AppState } from '../../app/app.state';


@Injectable()
export class TestDataservice {

  constructor(private testService: TestService, private appState: AppState) {
  }


  public getTests(): RequestReturn<TestVM[]> {
    return null;
  }

  public getTestByiD(id: number): RequestReturn<TestVM> {
    return null;
  }

  public addTest(test: TestVM): RequestReturn<boolean> {
    return null;
  }

  public editTest(test: TestVM): RequestReturn<boolean> {
    return null;
  }

  public executeTest(test: TestVM) : RequestReturn<TestResultVM> {
    return null;
  }

}