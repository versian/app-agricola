import { Injectable } from '@angular/core';
import { DatabaseService } from '../database-service/database-service';
import { UserVM } from '../../viewmodels/UserVM';


@Injectable()
export class UserService {

  constructor(private databaseService: DatabaseService) {
  }


  public getAll(): Promise<UserVM[]> {
    return this.databaseService.getDB()
      .then(db => {
        db.executeSql('SELECT * FROM User', {})
          .then(data => {
            return data.rows.items.map(function (user) {
              return new UserVM({
                 ID: user.ID, 
                 username: user.username, 
                 passwordHash: user.passwordHash, 
                 name: user.name, 
                 birthday: user.birthday, 
                 RG: user.RG, 
                 CPF: user.CPF, 
                 lastLogon: user.lastLogon,
                 loggedHash: user.loggedHash
                });
            });
          })
      })
      .catch(error => { return null; });
  }

  public getByID(id: number): Promise<UserVM> {
    return this.databaseService.getDB()
      .then(db => {
        db.executeSql('SELECT * FROM User WHERE id = ? LIMIT 1', [id])
          .then(data => {
            return data.rows.items.map(function (user) {
              return new UserVM({
                ID: user.ID, 
                username: user.username, 
                passwordHash: user.passwordHash, 
                name: user.name, 
                birthday: user.birthday, 
                RG: user.RG, 
                CPF: user.CPF, 
                lastLogon: user.lastLogon,
                loggedHash: user.loggedHash
               });
            });
          })
      })
      .catch(error => { return null; });
  }

  public insert(user: UserVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {
        return db.executeSql('INSERT INTO User VALUES((SELECT MAX(ID) + 1 FROM User), ?, ?, ?, ?, ?, ?, ?, NULL)', [user.username, user.passwordHash, user.name, user.birthday, user.RG, user.CPF, user.lastLogon])
              .then(result => { return true; })
              .catch(error => { return false; });
      })
      .catch(error => { return false; });
  }

  public update(user: UserVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {
        return db.executeSql('UPDATE User SET username = ?, passwordHash = ?, name = ?, birthday = ?, RG = ?, CPF = ?, loggedHash = ? WHERE id = ?', [user.username, user.passwordHash, user.name, user.birthday, user.RG, user.CPF, user.ID, user.loggedHash])
              .then(result => { return true; })
              .catch(error => { return false; });
      })
      .catch(error => { return false; });
  }

  public remove(user: UserVM): Promise<boolean> {
    return this.databaseService.getDB()
      .then(db => {
        return db.executeSql('DELETE User WHERE id = ?', [user.ID])
              .then(result => { return true; })
              .catch(error => { return false; });
      })
      .catch(error => { return false; });
  }

}
